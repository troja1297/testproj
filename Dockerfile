FROM python:3.10-slim-buster as base
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWIRTEBYTECODE=1 \
    PYSETUP_PATH=/opt/pysetup \
    APPLICATION_PATH=/usr/app
ENV VENV_PATH=$PYSETUP_PATH/.venv
ENV PATH=$VENV_PATH/bin:$PATH


FROM base as builder
ENV PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_HOME=/opt/poetry/ \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_NO_INTERACTION=1 \
    POETRY_VERSION=1.1.11
ENV PATH=$POETRY_HOME/bin:$PATH
WORKDIR $PYSETUP_PATH
RUN apt-get -y update \
    && apt-get install -y --no-install-recommends build-essential gettext libpq-dev curl \
    && curl -sSL https://install.python-poetry.org | python
COPY poetry.lock pyproject.toml ./
RUN poetry install -n --no-root --no-dev


FROM builder as dev-builder
RUN poetry install -n --no-root


FROM base as app-base
ENV PORT=8000 \
    MEDIA_ROOT=/media
EXPOSE 8000
WORKDIR $APPLICATION_PATH
RUN mkdir -p $STATIC_ROOT $MEDIA_ROOT


FROM app-base as development
ENV POETRY_HOME="/opt/poetry" \
    POETRY_NO_INTERACTION=1 \
    VIRTUAL_ENV=$VENV_PATH
ENV PATH="$POETRY_HOME/bin:$PATH"
COPY --from=dev-builder $POETRY_HOME $POETRY_HOME
COPY --from=dev-builder $VENV_PATH $VENV_PATH
COPY pyproject.toml poetry.lock manage.py docker-entrypoint.sh ./
COPY accounts/ ./accounts/
COPY core/ ./core/
COPY gallery/ ./gallery/
COPY templates/ ./template/
COPY static/ ./static/
RUN chmod +x docker-entrypoint.sh
RUN ls -al

RUN poetry install -n