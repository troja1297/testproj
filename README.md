# Globerce delivery backend services


## Requirements

The project requires the following system packages:
```
python = "^3.8"
Django = "^4.1.2"
Pillow = "^9.2.0"
psycopg2-binary = "^2.9.4"
gunicorn = "^20.1.0"
```

## Installation
```
docker-compose up --build
```


## DB migrations
 
For developing it's required to make and upgrade migrations with `python3 manage.py migrate` tool.

```
docker exec -it test_api_1 python3 manage.py migrate
```